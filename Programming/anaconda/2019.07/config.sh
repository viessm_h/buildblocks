# version-specific settings

# Miniconda version. Defaults to "latest"
# Should match the version distributed with anaconda
CONDA_VERSION=4.6.14

