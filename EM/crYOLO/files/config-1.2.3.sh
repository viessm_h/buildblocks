# crYOLO configuration variables

# Miniconda installer filename. Defaults to 'latest'
MINICONDA_VERSION='Miniconda2-4.5.12-Linux-x86_64.sh'

# cryoloBM download url
CRYOLOBM_URL='ftp://ftp.gwdg.de/pub/misc/sphire/crYOLO_BM_V1_1_1/cryoloBM-1.1.1.tar.gz'
# cryolo download url. Default:
#CRYOLO_URL='ftp://ftp.gwdg.de/pub/misc/sphire/crYOLO_V1_2_3/cryolo-1.2.3.tar.gz'

