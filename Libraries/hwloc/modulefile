#%Module1.0

module-whatis		"The Portable Hardware Locality (hwloc) software package"
module-url		"https://www.open-mpi.org/projects/hwloc/"
module-license		"Open source, see $PREFIX/share/doc/hwloc/COPYING"
module-maintainer	"Marc Caubet Serrabou <marc.caubet@psi.ch>"

module-help		"
The Portable Hardware Locality (hwloc) software package provides a 
portable abstraction (across OS, versions, architectures, ...) of the
hierarchical topology of modern architectures, including NUMA memory 
nodes, sockets, shared caches, cores and simultaneous multithreading. 

It also gathers various system attributes such as cache and memory 
information as well as the locality of I/O devices such as network 
interfaces, InfiniBand HCAs or GPUs.

Hwloc primarily aims at helping applications with gathering information
about increasingly complex parallel computing platforms so as to exploit
them accordingly and efficiently.

Hwloc may also help many applications just by providing a portable CPU 
and memory binding API and a reliable way to find out how many cores 
and/or hardware threads are available. 
"
